<?php
$userID = $_GET['userid']; // get user id from url
$postID = $_GET['postid']; // get post id from url
$text = $_POST["commentArea"];

require_once 'postFuncs.php';

addComment($userID, $postID, $text);

header("Location: /src/php/viewBlogPost.php?postid=$postID");

?>