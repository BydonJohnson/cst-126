<?php
//   Milestone-1 
//   ver. 1 
//   Joshua W., Noah R., Brydon J.

//   getUserDetailsHandler.php: 
//      

//   getUserDetailsHandler.php: 
//      used to obtain the details of a user 

require 'adminFuncs.php';
$userData = getUserDetails($_POST["getUserDetailsID"]);

if ($userData){
  echo "User ID: ".$userData["USER_ID"] . '<br>' .
  "First Name: ".$userData["FIRST_NAME"] . '<br>' .
  "Last Name: ".$userData["LAST_NAME"] . '<br>' .
  "Username: ".$userData["USERNAME"] . '<br>' .
  "Password: ".$userData["PASSWORD"] . '<br>' .
  "Email: ".$userData["EMAIL"] . '<br>' .
  "Address: ".$userData["ADDRESS"] . '<br>' .
  "City: ".$userData["CITY"] . '<br>' .
  "State: ".$userData["STATE"] . '<br>' .
  "Zip: ".$userData["ZIPCODE"] . '<br>' .
  "Country: ".$userData["COUNTRY"] . '<br>' .
  "RoleID (1 = Admin, 2 = User, 3 = mod): ".$userData["ROLE_ID"] . '<br>' . 
  '<a href="editUserInfoForm.php?id='. $userData["USER_ID"].'" class="button">Edit User Information</a>';


} else {
  echo "Invalid User ID";
}
?>