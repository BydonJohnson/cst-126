<?php

//   Milestone-1 
//   ver. 1 
//   Joshua W., Noah R., Brydon J.

//   adminFuncs.php: 
//      this php script defines all the functions that are
//      to be used by the administrator in order to manage
//      the data base of users/posts/roles/etc.

// create database connection
require 'myFuncs.php';
$conn = dbConnect();

function checkIfCurrentUserIsAdmin(){
  
  echo 'test 1 <br>';
  global $conn;

  // get the user id of the current user
  $currentUserID = getUserID();
  echo 'test 2 <br>';
  if($currentUserID != ""){ // if user is logged in

    echo 'test 3 <br>';
    // get the rold id of the current user
    $query = "SELECT ROLE_ID FROM usersDB WHERE USER_ID = '$currentUserID';";
    $result = sqlsrv_query($conn, $query, array(), array( "Scrollable" => 'static' ));
    $row = sqlsrv_fetch_array($result);
    $role = $row["ROLE_ID"];
    echo 'test 4 role: ' . $role . '<br>';

    if($role == 1){ // return true if current user is admin
      echo 'test 5 true admin <br>';
      return true;
    } else { // return false if current user is not admin
      echo 'test 6 false not admin <br>';
      return false;
    }
  } else { // return false if user is not logged in
    echo 'test 7 not logged in <br>';
    return false;
  }
}


// delete a post
function deletePost($postID){
  global $conn;

  $deleteQuery = "DELETE FROM postsDB WHERE POST_ID = '$postID';";

  $deleteResult = sqlsrv_query($conn, $deleteQuery, array(), array( "Scrollable" => 'static' ));
  
  $numOfRows = sqlsrv_num_rows($deleteResult);
      
  if($numOfRows == 1){ //successful login
    echo "Post successfully deleted";
  } else {
    echo "No post was found with postID: " . $postID;
  }
}  



// delete a user?
function deleteUser($userID){
  global $conn;

  $deletePostQuery = "DELETE FROM postsDB WHERE POSTED_BY = '$userID';";
  $deleteUserQuery = "DELETE FROM usersDB WHERE USER_ID = '$userID';";

  $deletePosts = sqlsrv_query($conn, $deletePostQuery, array(), array( "Scrollable" => 'static' ));
  $deleteUsers = sqlsrv_query($conn, $deleteUserQuery, array(), array( "Scrollable" => 'static' ));

  $numOfRows = sqlsrv_num_rows($deleteUsers);

  if($numOfRows == 1){
    echo "User was deleted <br>";
  } else if( $numOfRows == 0){
    echo "No user was found with userID: " . $userID;
  }
} 

// update a post
function updatePost($postID){
  global $conn;

} 

// READ user information
function getUserDetails($userID){
  global $conn;
  $grabUser = "SELECT * FROM usersDB WHERE USER_ID = '$userID';";
  $userResult = sqlsrv_query($conn, $grabUser, array(), array("Scrollable" => 'static'));
  $userRow = sqlsrv_fetch_array($userResult);
  return $userRow;
} 

// edit user details
function UpdateUserInfo($userID, $firstName, $lastName, $username, $password, $email, $address, $city, $state, $zip, $country, $role){
  global $conn;

  if ($role == "User") {
    $role = 2;
  } else{
    $role = 1;
  }

  $updateQuery = "Update usersDB 
                  SET FIRST_NAME = '$firstName', LAST_NAME = '$lastName', 
                  USERNAME = '$username', PASSWORD = '$password', 
                  EMAIL = '$email', ADDRESS = '$address',
                  CITY = '$city', STATE = '$state',
                  ZIPCODE = '$zipcode', COUNTRY = '$country', 
                  ROLE_ID = '$role'
                  WHERE USER_ID = '$userID';";

  sqlsrv_query($conn, $updateQuery);

}

?>
