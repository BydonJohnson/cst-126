<?php
//   Milestone
//   ver. 1 
//   Joshua W., Noah R., Brydon J.

//   selectPhotoHandler.php: 
//      this php script is used to take the photo path 
//      of the photo selected by the user and store it
//      to the database as PHOTO_PATH value

//

// name="img-selection
require_once 'postFuncs.php';

// store the path of the image that was selected
$photoPath = $_POST["img-selection"]; 
$postID = $_GET[postid];

addPhoto($postID, $photoPath);


?>