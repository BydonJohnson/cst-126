<?php

require 'postFuncs.php';
require_once 'myFuncs.php';

$currentUserID = getUserID();

$postID = $_GET[postid];

// get info for the post
$post = getBlogPost($postID);
$postTitle = $post['POST_TITLE'];
$postImagePath = $post['PHOTO_PATH'];
$postContent = $post['POST_CONTENT'];

// get comments for the post
$commentsArray = getAllCommentsForPost($postID);

$commentHandlerPath = "/src/php/commentHandler.php?userid=" . $currentUserID . "&postid=" . $postID;
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="/src/css/style.css">
  <link rel="stylesheet" href="/src/css/viewPostStyle.css">
</head>
<body>
  <header>
    <div class="header-title-container">
      <div class="header-title-text"><span>BLOG</span>JBN</div>
    </div>
    <div class="header-navigation-container">
      <div class="nav-btn-container">
        <a href="/index.html" class="nav-btn">Home</a>
      </div>
      <div class="nav-btn-container">
        <a href="/login.html" class="nav-btn">Login</a>
      </div>
      <div class="nav-btn-container">
        <a href="/src/php/logoutHandler.php" class="nav-btn">Log Out</a>
      </div>
      <div class="nav-btn-container">
        <a href="/register.html" class="nav-btn">Sign-Up</a>
      </div>
      </div>
    </div>
  </header>
  <div class="full-wrapper">
    <div class="outer-wrapper">
      <div class="inner-wrapper">
        <div class="post-image">
          <img src="<?php echo $postImagePath;?>">
        </div>
        <div class="post-title">
          <?php echo $postTitle;?>
        </div>
        <div class="post-content">
          <?php echo $postContent;?>
        </div>
      </div>
      <h1>Comments</h1>
      <div class="display-comments-wrapper">
        <?php 
          $num_comments = count($commentsArray);
          for ($i=0; $i < $num_comments; $i++) { 
            $row = $commentsArray[$i];
            echo displayComment($row['COMMENT_ID']);

          }
        ?>
      </div>

      <?php
      if($currentUserID > 0){
        echo'
          <div class="write-comment-wrapper">
            <form action="' . $commentHandlerPath . '" method="POST" action="_blank">
              <label for="commentArea">Add a comment:</label>
              <br>
              <textarea name="commentArea" id="commentArea" cols="30" rows="10"></textarea>
              <br>
              <input type="submit" value="Comment">
            </form>
          </div>  ';
      } else {
        echo'
          <div class="write-comment-wrapper">
            please login to add a comment to this post
          </div>  ';
      }
      ?>
    </div>
  </div>
</body>
</html>