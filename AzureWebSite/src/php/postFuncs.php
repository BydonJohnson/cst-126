<?php

//   Milestone-1 
//   ver. 1 
//   Joshua W., Noah R., Brydon J.

//   postFuncs.php: 
//      this php script provides some funtions to be used to interact with the posts that users make

//      StorePost($userID, $postTitle, $postCategory, $postString): stores the post to the sql database
//			filterwords($text): filters the words in the parameter string. returns the same string with bad words converted to ***

require_once 'myFuncs.php';
$conn = dbConnect();

// created database connection, then stores the post to our SQL database table 'posts'
function StorePost($userID, $postTitle, $postCategory, $postString)
{
	global $conn;

	$sqlInsert = "INSERT INTO postsDB (POSTED_BY, POST_TITLE, CATEGORY, POST_CONTENT)
				VALUES('$userID', '$postTitle', '$postCategory', '$postString');";

	$queryResult = sqlsrv_query($conn, $sqlInsert);
	
	if ($queryResult === false) {
    die(print_r(sqlsrv_errors(), true));
	} else {
		echo "New post stored Successfully";

		// write query to find the post we just created
		$sqlSelect = "SELECT POST_ID FROM postsDB
									WHERE POSTED_BY = '$userID' 
									AND POST_TITLE = '$postTitle'
									AND CATEGORY = '$postCategory'
									AND POST_CONTENT = '$postString'";

		$result = sqlsrv_query($conn, $sqlSelect, array(), array( "Scrollable" => 'static' ));
		$row = sqlsrv_fetch_array($result);
		$postID = $row["POST_ID"];
		return $postID;
	}
}

// add photo path to a specified post
function addPhoto($postID, $photoPath){
	global $conn;

	$query = "UPDATE postsDB
						SET PHOTO_PATH = '$photoPath'
						WHERE POST_ID = '$postID';";

	$result = sqlsrv_query($conn, $query, array(), array( "Scrollable" => 'static' ));

	if($result){
		echo 'photo successfully added';
	}

}

// takes in a string parameter and filters out a selection from that string.
// returns the string with those words replaced with "***"
function filterwords($text){
	$filterWords = array('gosh', 'darn', 'poo', 'shmear', 'shucks');
	$filterCount = sizeof($filterWords);
	for ($i = 0; $i < $filterCount; $i++) {
		$text = preg_replace_callback('/\b' . $filterWords[$i] . '\b/i', function($matches){return str_repeat('*', strlen($matches[0]));}, $text);
	}
	return $text;
}

function getBlogPost($postID){
	global $conn;
	
	$query = "SELECT * FROM postsDB WHERE POST_ID = '$postID';";
	
	$queryResult = sqlsrv_query($conn, $query);

	$row = sqlsrv_fetch_array($queryResult);
	return $row;
}

// this function is used to wite the html format to display the post in the UI
function createPostCard($post){
	$html = '
	<a href="/src/php/viewBlogPost.php?postid='.$post["POST_ID"].'">
		<div class="post_wrapper">
			<div class="post_card">
				<div class="post_card_image">
					<img src="' . $post["PHOTO_PATH"] . '" alt="coding image">
				</div>
				<div class="post_title_and_content">
					<div class="post_card_postID">POST_ID: ' . $post["POST_ID"] . '</div>' .
					'<div class="post_card_postedBY">POST_BY (id): ' . $post["POSTED_BY"] . '</div>' .
					'<div class="post_card_title">' . $post["POST_TITLE"] . '</div>
					<div class="post_card_content">' . $post["POST_CONTENT"] . '</div>
				</div>
			</div> <!-- end post card -->
		</div> 
	</a>';
	
	return $html;
}

// used to add a comment to a post
function addComment($userID, $postID, $text){
	global $conn;

	echo $userID . ' ' . $postID . ' ' . $text;

	$query = "INSERT INTO comments (POSTED_BY, POST_ID, COMMENT_TEXT)
						VALUES ('$userID', '$postID', '$text');";

	sqlsrv_query($conn, $query, array(), array( "Scrollable" => 'static' ));
}

// used to get an array of all comments for a single post
function getAllCommentsForPost($postID){
	global $conn;

	// use array to store post
	$commentArray = array();

	$query = "SELECT * FROM comments WHERE POST_ID = '$postID';";
	$result = sqlsrv_query($conn, $query, array(), array( "Scrollable" => 'static' ));
	
	if($result){
		// save number of rows so we don't execute if we don't have any comments on this post
		$num_rows = sqlsrv_num_rows($result);
		if($num_rows > 0){
			for ($i=0; $i < $num_rows; $i++) { 
				$row = sqlsrv_fetch_array($result);
				$commentArray[$i] = $row;
      }	
		}	else {
			echo "no comments";
		}
	}
	return $commentArray;
}

// used to retrieve information for a specific comment
function getComment($commentID){
	global $conn;
	
	$query = "SELECT * FROM comments WHERE COMMENT_ID = '$commentID';";
	
	$queryResult = sqlsrv_query($conn, $query);

	$row = sqlsrv_fetch_array($queryResult);
	return $row;
}

// this function is used to create the comments displayed beneath a post when being viewed
function displayComment($commentID){
	global $conn;

	$comment = getComment($commentID);
	$userID = $comment['POSTED_BY'];
	
	$query = "SELECT FIRST_NAME, LAST_NAME
						FROM usersDB 
						WHERE USER_ID = '$userID'";

	$result = sqlsrv_query($conn, $query, array(), array( "Scrollable" => 'static' ));
	$row = sqlsrv_fetch_array($result);
	$firstName = $row["FIRST_NAME"];
	$lastName = $row["LAST_NAME"];



	$html = '
		<div class="comment-wrapper">
			<div class="comment-author">' . $firstName . ' ' . $lastName . '</div>
			<div class="comment-content">'.$comment['COMMENT_TEXT'].'</div>
		</div>';

	return $html;
}

?>

