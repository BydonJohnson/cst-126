<!-- 
  Milestone-1 
  ver. 1 
  Joshua W., Noah R., Brydon J.

  postHandler.php: 
    This php script accepts the user input from post.html and uses the StorePost 
    frunction from postFuncs.php to store a post into our posts sql table 
  
  -->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>postHandler php</title>
    <link rel="stylesheet" href="/src/css/style.css">
</head>
<body>
  <header>
    <div class="header-title-container">
      <div class="header-title-text"><span>BLOG</span>JBN</div>
    </div>
    <div class="header-navigation-container">
      <div class="nav-btn-container">
        <a href="/index.html" class="nav-btn">Home</a>
      </div>
      <div class="nav-btn-container">
        <a href="/login.html" class="nav-btn">Login</a>
      </div>
      <div class="nav-btn-container">
        <a href="/src/php/logoutHandler.php" class="nav-btn">Log Out</a>
      </div>
      <div class="nav-btn-container">
        <a href="/register.html" class="nav-btn">Sign-Up</a>
      </div>
      </div>
    </div>
  </header>
</body>
</html>

<?php

  require_once 'myFuncs.php';
  $userID = getUserID();

  if( $userID != "" ){
    require_once 'postFuncs.php';

    $postTitle = $_POST["postTitle"];
    $postCategory = $_POST['category'];
    $postString = $_POST["blogPost"];

    $filteredString = FilterWords($postString);
    $postID = StorePost($userID, $postTitle, $postCategory, $filteredString);

    echo '<br><a href="/src/php/selectPhoto.php?postid=' . $postID . '">Add a photo to your post!</a>';


  } else {
    echo 'please log in before posting';
  }


?>