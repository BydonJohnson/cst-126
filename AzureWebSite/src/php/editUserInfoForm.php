  <!-- Milestone-1 
  ver. 1 
  Joshua W., Noah R., Brydon J.

  getUserDetailsHandler.php: 
    used to update the details of a user  -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>registerHandler php</title>
    <link rel="stylesheet" href="/src/css/style.css">
</head>
<body>
  <header>
    <div class="header-title-container">
      <div class="header-title-text"><span>BLOG</span>JBN</div>
    </div>
    <div class="header-navigation-container">
      <div class="nav-btn-container">
        <a href="/index.html" class="nav-btn">Home</a>
      </div>
      <div class="nav-btn-container">
        <a href="/login.html" class="nav-btn">Login</a>
      </div>
      <div class="nav-btn-container">
        <a href="/src/php/logoutHandler.php" class="nav-btn">Log Out</a>
      </div>
      <div class="nav-btn-container">
        <a href="/register.html" class="nav-btn">Sign-Up</a>
      </div>
      </div>
    </div>
  </header>
</body>
</html>

<?php 

require 'adminFuncs.php';
$userID = $_GET['id'];
$userInfo = getUserDetails($userID);

$firstName = $userInfo["FIRST_NAME"];

echo '
<form action="/src/php/editUserInfoHandler.php?id='.$userID.'" method="POST">
<label for="FirstName">First Name</label>
<input type="text"name="FirstName"id="FirstName"value="'.$firstName.'"required pattern="[a-ZA-Z]+"> <br>
    <label for="LastName">Last Name</label>
    <input type="text" name="LastName" id="LastName" value="' . $userInfo["LAST_NAME"] . '" required pattern="[a-zA-Z]+">
    <hr>

    <label for="Username">Username</label>
    <input type="text" name="Username" id="Username" value="' . $userInfo["USERNAME"] . '" required minlength="3" maxlength="15" pattern="[a-zA-Z0-9]+"> <br>
    <label for="Password">Password</label>
    <input type="password" name="Password" id="Password" value="' . $userInfo["PASSWORD"] . '" required minlength="8" maxlength="15">
    <hr>

    <label for="Email">Email</label>
    <input type="email" name="Email" id="Email" value="' . $userInfo["EMAIL"] . '" required>
    <hr>

    <!--Form Data for users address, city, state, country, and zip code-->
    <label for="Address">Address (optional)</label>
    <input type="text" name="Address" id="Address" value="' . $userInfo["ADDRESS"] . '" ><br>
    <label for="City">City (optional)</label>
    <input type="text" name="City" id="City" value="' . $userInfo["CITY"] . '"><br>
    <label for="State">State (optional)</label>
    <select name="State" id="State">
      <option value=" "> </option>
      <option value="AL">Alabama</option>
      <option value="AK">Alaska</option>
      <option value="AZ">Arizona</option>
      <option value="AR">Arkansas</option>
      <option value="CA">California</option>
      <option value="CO">Colorado</option>
      <option value="CT">Connecticut</option>
      <option value="DE">Delaware</option>
      <option value="DC">District Of Columbia</option>
      <option value="FL">Florida</option>
      <option value="GA">Georgia</option>
      <option value="HI">Hawaii</option>
      <option value="ID">Idaho</option>
      <option value="IL">Illinois</option>
      <option value="IN">Indiana</option>
      <option value="IA">Iowa</option>
      <option value="KS">Kansas</option>
      <option value="KY">Kentucky</option>
      <option value="LA">Louisiana</option>
      <option value="ME">Maine</option>
      <option value="MD">Maryland</option>
      <option value="MA">Massachusetts</option>
      <option value="MI">Michigan</option>
      <option value="MN">Minnesota</option>
      <option value="MS">Mississippi</option>
      <option value="MO">Missouri</option>
      <option value="MT">Montana</option>
      <option value="NE">Nebraska</option>
      <option value="NV">Nevada</option>
      <option value="NH">New Hampshire</option>
      <option value="NJ">New Jersey</option>
      <option value="NM">New Mexico</option>
      <option value="NY">New York</option>
      <option value="NC">North Carolina</option>
      <option value="ND">North Dakota</option>
      <option value="OH">Ohio</option>
      <option value="OK">Oklahoma</option>
      <option value="OR">Oregon</option>
      <option value="PA">Pennsylvania</option>
      <option value="RI">Rhode Island</option>
      <option value="SC">South Carolina</option>
      <option value="SD">South Dakota</option>
      <option value="TN">Tennessee</option>
      <option value="TX">Texas</option>
      <option value="UT">Utah</option>
      <option value="VT">Vermont</option>
      <option value="VA">Virginia</option>
      <option value="WA">Washington</option>
      <option value="WV">West Virginia</option>
      <option value="WI">Wisconsin</option>
      <option value="WY">Wyoming</option>
    </select>	<br>
    <label for="Zipcode">Zipcode (optional)</label>
    <input type="text" name="Zipcode" id="Zipcode" value="' . $userInfo["ZIPCODE"] . '" minlength="5" maxlength="5" pattern="[0-9]+"> <br>
    <label for="Country">Country (optional)</label>
    <input type="text" name="Country" id="Country" value="' . $userInfo["COUNTRY"] . '"><br>

    <label for="Role">Select Role:</label>
    <input type="radio" name="Role" value="Administrator">Administrator
    <input type="radio" name="Role" value="User" checked>User
    <br>
    <input type="submit" value="Submit">
  </form>



';

?>