<?php

require_once 'myFuncs.php';
$conn = dbConnect();

$postID = $_GET[postid];

// $sqlSelect = "SELECT POST_ID FROM postsDB
// 							WHERE POSTED_ID = '$userID'";

// 		$result = sqlsrv_query($conn, $sqlSelect);
// 		$row = sqlsrv_fetch_array($result);
// 		$postID = $row["POST_ID"];
// 		return $postID;

echo'
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/src/css/testSelectPhotoStyle.css">
  <title>Document</title>
</head>
  <header>
    <div class="header-title-container">
      <div class="header-title-text"><span>BLOG</span>JBN</div>
    </div>
    <div class="header-navigation-container">
      <div class="nav-btn-container">
        <a href="/index.html" class="nav-btn">Home</a>
      </div>
      <div class="nav-btn-container">
        <a href="/login.html" class="nav-btn">Login</a>
      </div>
      <div class="nav-btn-container">
        <a href="/src/php/logoutHandler.php" class="nav-btn">Log Out</a>
      </div>
      <div class="nav-btn-container">
        <a href="/register.html" class="nav-btn">Sign-Up</a>
      </div>
      </div>
    </div>
  </header>
<body>
  <form action="/src/php/selectPhotoHandler.php?postid=' . $postID . '" method="POST">
    <div class="outer-wrapper">
      <div class="inner-wrapper">
        <div class="section-header-container">
          <div class="section-header">Art</div>
        </div>
        <div class="img-option-outer-container">
          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/art/art1.jpg" checked>
              <img src="/img/art/art1.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/art/art2.jpg">
              <img src="/img/art/art2.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/art/art3.jpg">
              <img src="/img/art/art3.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/art/art4.jpg">
              <img src="/img/art/art4.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/art/art5.jpg">
              <img src="/img/art/art5.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/art/art6.jpg">
              <img src="/img/art/art6.jpg">
            </label>
          </div>

        </div> <!-- end outer img option container -->

        <div class="section-header-container">
          <div class="section-header">Science</div>
        </div>

        <!-- start science photos -->
        <div class="img-option-outer-container">
          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/science/science1.jpg">
              <img src="/img/science/science1.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/science/science2.jpg">
              <img src="/img/science/science2.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/science/science3.jpg">
              <img src="/img/science/science3.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/science/science4.jpg">
              <img src="/img/science/science4.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/science/science5.jpg">
              <img src="/img/science/science5.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/science/science6.jpg">
              <img src="/img/science/science6.jpg">
            </label>
          </div>

        </div> <!-- end outer img option container -->

        <div class="section-header-container">
          <div class="section-header">Business</div>
        </div>

        <!-- start business photos -->
        <div class="img-option-outer-container">
          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/business/business1.jpg">
              <img src="/img/business/business1.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/business/business2.jpg">
              <img src="/img/business/business2.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/business/business3.jpg">
              <img src="/img/business/business3.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/business/business4.jpg">
              <img src="/img/business/business4.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/business/business5.jpg">
              <img src="/img/business/business5.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/business/business6.jpg">
              <img src="/img/business/business6.jpg">
            </label>
          </div>

        </div> <!-- end outer img option container -->

        <div class="section-header-container">
          <div class="section-header">Travel</div>
        </div>

        <!-- start travel photos -->
        <div class="img-option-outer-container">
          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/travel/travel1.jpg">
              <img src="/img/travel/travel1.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/travel/travel2.jpg">
              <img src="/img/travel/travel2.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/travel/travel3.jpg">
              <img src="/img/travel/travel3.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/travel/travel4.jpg">
              <img src="/img/travel/travel4.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/travel/travel5.jpg">
              <img src="/img/travel/travel5.jpg">
            </label>
          </div>

          <div class="img-option-inner-container">
            <label>
              <input type="radio" name="img-selection" value="/img/travel/travel6.jpg">
              <img src="/img/travel/travel6.jpg">
            </label>
          </div>

        </div> <!-- end outer img option container -->
      </div> <!-- end inner wrapper -->
    </div> <!-- end outer wrapper -->


    <div class="submit-container">
      <input type="submit" value="Continue">
    </div>
  </form>


</body>
</html>
';

?>