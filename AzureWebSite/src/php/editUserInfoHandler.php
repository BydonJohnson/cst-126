<!--    
  Milestone-1 
  ver. 1 
  Joshua W., Noah R., Brydon J.

  editUserInfoHandler.php: 
      
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>registerHandler php</title>
    <link rel="stylesheet" href="/src/css/style.css">
</head>
<body>
  <header>
    <div class="header-title-container">
      <div class="header-title-text"><span>BLOG</span>JBN</div>
    </div>
    <div class="header-navigation-container">
      <div class="nav-btn-container">
        <a href="/index.html" class="nav-btn">Home</a>
      </div>
      <div class="nav-btn-container">
        <a href="/login.html" class="nav-btn">Login</a>
      </div>
      <div class="nav-btn-container">
        <a href="/src/php/logoutHandler.php" class="nav-btn">Log Out</a>
      </div>
      <div class="nav-btn-container">
        <a href="/register.html" class="nav-btn">Sign-Up</a>
      </div>
      </div>
    </div>
  </header>
</body>
</html>

<?php
require 'adminFuncs.php';

$userID = $_GET['id'];
$firstName = $_POST["FirstName"];
$lastName = $_POST["LastName"];
$username = $_POST["Username"];
$password = $_POST["Password"];
$email = $_POST["Email"];
$address = $_POST["Address"];
$city = $_POST["City"];
$state = $_POST["State"];
$zip = $_POST["Zipcode"];
$country = $_POST["Country"];
$role = $_POST["Role"];

echo $userID . ' - ' . $firstName . ' - ' . $lastName . ' - ' . $username . ' - ' . $password
. ' - ' . $email . ' - ' . $address . ' - ' . $city . ' - ' . $state . ' - ' . $zip
. ' - ' . $country . ' - ' . $role;

UpdateUserInfo($userID, $firstName, $lastName, $username, $password, $email, $address, $city, $state, $zip, $country, $role);
?>
