<!--    
  Milestone-1 
  ver. 1 
  Joshua W., Noah R., Brydon J.

  searchHandler.php: 
      When the user enters a search criteria, this file is used to search through the
      sql database and retrieve the records that contain the words from the
      search in either their title, or their content.
-->

<?php

require_once 'myFuncs.php';
$conn = dbConnect();

require 'postFuncs.php';

$search_params = $_POST['search-params'];

// array to store all the posts matching the search criterion
$postArray = array();

// query to get all relevent posts
// $query = "SELECT * FROM postsDB WHERE CATEGORY = 'art';";
$query = "SELECT * FROM postsDB 
          WHERE POST_TITLE LIKE '%$search_params%'
          OR POST_CONTENT LIKE '%$search_params%';";

// save the results of this query to a variable
$results = sqlsrv_query($conn, $query, array(), array( "Scrollable" => 'static' ));


if($results){

  // save number of rows so we don't execute if we don't have any posts in this category
  $num_rows = sqlsrv_num_rows($results);
  
  if($num_rows > 0){
      for ($i=0; $i < $num_rows; $i++) { 
        $postArray[$i] = sqlsrv_fetch_array($results);
      }
  }
} else {
  echo "Error: " .$query . "<br>" . $conn->error;
}
// $post = getBlogPost(9);

// $post_card = createPostCard($post);

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/src/css/postStyle.css">
  <link rel="stylesheet" href="/src/css/style.css">

  <title>Document</title>
</head>
<body>

  <header>
    <div class="header-title-container">
      <div class="header-title-text"><span>BLOG</span>JBN</div>
    </div>
    <div class="header-navigation-container">
      <div class="nav-btn-container">
        <a href="/index.html" class="nav-btn">Home</a>
      </div>
      <div class="nav-btn-container">
        <a href="/login.html" class="nav-btn">Login</a>
      </div>
      <div class="nav-btn-container">
        <a href="/src/php/logoutHandler.php" class="nav-btn">Log Out</a>
      </div>
      <div class="nav-btn-container">
        <a href="/register.html" class="nav-btn">Sign-Up</a>
      </div>
      </div>
    </div>
  </header>

  <!-- adding search form to the search page above search results -->
  <form action="/src/php/searchHandler.php" class="search-form" method="POST">
        <label for="search-params">Search Blog Posts:</label>
        <input type="text" name="search-params" id="search-params">
        <input type="submit" value="Search">
  </form>

  <div class="outer_posts_container">
    <div class="inner_posts_container inner_posts_container--art">
      <?php 
      if(count($postArray) > 0){ // if there was a post or multiple matching the search
        foreach ($postArray as $p) {
          echo createPostCard($p);
        }
      } else { // there was no post matching the search
        echo 'no results found!';
      }
      ?>
    </div>
  </div>


</body>
</html>