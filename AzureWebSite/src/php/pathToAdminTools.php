<?php
require_once 'adminFuncs.php';
require_once 'myFuncs.php';

if(checkIfCurrentUserIsAdmin()){ // if user is admin link to admin tools page
  header("Location: /administratorTools.html");
} else { // user is not admin
  echo 'you must be logged in as an administrator to access this part of the site.<br>' .
        'Try user1 password1 to login as admin';
}
?>