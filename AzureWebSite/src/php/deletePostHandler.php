<?php

//   Milestone-1 
//   ver. 1 
//   Joshua W., Noah R., Brydon J.

//   deletePostHandler.php: 
//      used to process the request to delete a post form the database
//      checks first if the user calling the function is an admin

// create database connection

require_once 'adminFuncs.php';


$postID = $_POST["deleteBlog"];
deletePost($postID);


?>